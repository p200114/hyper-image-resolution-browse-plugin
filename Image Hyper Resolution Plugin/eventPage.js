var contextMenuItem = {
    "id": "improves",
    "title": "Enhance",
    "contexts": ["image"]
};
chrome.contextMenus.create(contextMenuItem);

chrome.contextMenus.onClicked.addListener(function(onclickData){
    var algorithm_d ="esrgan";
    chrome.storage.sync.get(['algorithmChoice','url_ch'], function(algorithm){
        server_address = `http://localhost:5000`
        console.log(algorithm.algorithmChoice)
        if(algorithm.url_ch)
        {
            server_address = algorithm.url_ch;
        }
        
        algorithm_default = algorithm.algorithmChoice;
	if(!algorithm_default){
	  algorithm_default = "esrgan";
	}

        if(onclickData.menuItemId == "improves")
        {
            var newUrl = onclickData.srcUrl;
            var api_url = `${server_address}/improve_image?url=${newUrl}&algorithm=${algorithm_d}`
           // var api_url = `localhost:5000/improve_image?url=${newUrl}&algorithm=${algorithm_d}`
            chrome.tabs.create({url:api_url});
        }
    });
    
    
}); 
