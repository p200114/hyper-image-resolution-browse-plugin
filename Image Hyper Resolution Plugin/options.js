$(function(){
    $('#SaveChoice').click(function(){
        var algorithmChoice = $('#algorithm').val();
        var url_choice = $('#url_file').val();
        if(algorithmChoice){
            chrome.storage.sync.set({'algorithmChoice':algorithmChoice, 'url_ch':url_choice}, function(){
                var algolChoice = {
                    type: 'basic',
                    iconUrl: 'icon48.png',
                    title: 'Extension option changed',
                    message: 'Extension algorithm changed'   
                };
                chrome.notifications.create('algol', algolChoice);
                close();
            });
        }
    })
});


