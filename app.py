from flask import Flask, render_template, send_from_directory
from ISR.models import RDN
from ISR.models import RRDN
from PIL import Image
from flask_ngrok import run_with_ngrok
from flask import request
import random, string, filetype, imghdr, requests, cv2, os, http.client
import numpy as np 
from urllib.request import urlopen, URLError
import config as cfg
from image_processor import ISR
from esrgan import esrgan 

app = Flask(__name__)
#run_with_ngrok(app)
#turn off the comment on the above line of code to expose the local server to the interner

@app.route('/home/')
def home_page():
    return render_template("home.html")
@app.route('/')
def home():
    return render_template("home.html")
@app.route('/hello/<int:score>')
def hello_name(score):
    return render_template('hello.html', marks = score)
@app.route('/processed', methods=['POST'])
def handleData():
    image_str = request.files['image_file_input']
    filename = image_str.filename
    save_location = os.path.join(cfg.image_uploads, image_str.filename)
    ip = ISR.image_improve_all
    esrgans = esrgan.Esrgan
    file_detail, file_extension = os.path.splitext(filename)
    if(file_extension == ".png" or file_extension == ".PNG"):
        file_detail = file_detail + ".jpg"
        location = os.path.join(cfg.image_uploads, file_detail)
        image_str.save(save_location)
        im = Image.open(save_location)
        rgb_im = im.convert('RGB')
        rgb_im.save(location)
    else:
        location = os.path.join(cfg.image_uploads, image_str.filename)
        image_str.save(location) 
           
    psnrlr  = "psnrlr"
    psnrsm = "psnrsm"
    gans = "gans"
    noise = "noiseCancel"
    improved_image ={}

    value = request.form['radio']
    if request.form.get("select_all"):
        
        gans_image = ip(location, gans)
        print(gans_image)
    else:
       if value == "psnrlr"  or value== "all":
           
           improved_image["Improved using PSNR-LR"] = ip(location,psnrlr)
       if value == "psnrsm" or value=="all":
            
           improved_image["Improved using PSNR-SM"] = ip(location,psnrsm)
       if value == "gan" or value=="all":
           improved_image["Improved using GAN"] = ip(location, gans)
       if value == "noise" or value=="all":
           improved_image["improved Using Artifact cancelling"] = ip(location, noise)
       if value == "esrgan" or value=="all":
           improved_image["Improved using ESRGAN"] = esrgans.image_improve_esrgan   (location)
  
    original_location = '/static/original_image/' + image_str.filename

    improved_image['Original image'] = original_location
    
    return  render_template("improved_image.html", user_image = improved_image)

@app.route('/download', methods = ['GET'])
def download():
    filename = request.args.get('file', None)
    path, file_n = os.path.split(filename)
    path = path + "/"
    path = path[1:]
    return send_from_directory(directory = path, filename = file_n, as_attachment=True)     

@app.route('/improve_image', methods = ['GET'])
def improve_test():
    #print(url)
    url = request.args.get("url")
    algorithm = request.args.get("algorithm")
    filename_url = url.split('/')[-1]
    file_detail, file_extension = os.path.splitext(filename_url)
    
    ip = ISR.image_improve_all
    urlCheck = ISR.url_check
    fileTypeCheck = ISR.check_file_type
    esrgans = esrgan.Esrgan

    random_string = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
    new_filename = algorithm + "_web_image_" + random_string + file_extension
    result_image ={}   
    
    new_filename_save = 'static/web_images/' + new_filename
    if urlCheck(url):
        r = requests.get(url, allow_redirects=True)
        location = os.path.join(cfg.web_images, new_filename)
        with open(new_filename_save, 'wb') as f:
             f.write(r.content)
        print("i am here")
        if fileTypeCheck(new_filename_save):
                if algorithm != "esrgan": 
                    print("i am here too")
                    if(file_extension == ".png" or file_extension == ".PNG"):
                            new_filename = algorithm + "_web_image_" + random_string + random_string + ".jpg"
                            new_filename = 'static/web_images/' + new_filename 
                            im = Image.open(new_filename_save)
                            rgb_im = im.convert('RGB')
                            rgb_im.save(new_filename)
                    Algol_name = "improved Using " + algorithm 
                    result_image[Algol_name] = ip(new_filename, algorithm)
                    result_image["Original Image"] = new_filename_save
                    return render_template("Improved_image.html", user_image = result_image)
                else:   
                    result_image["Improved Using ESRGAN"] = esrgans.image_improve_esrgan(new_filename_save)
                    result_image["Original Image"] = new_filename
                    return render_template("Improved_image.html", user_image = result_image) 
        else:
            print("not here")
            return render_template("error.html")
    else:
        return render_template("error.html")
    
 
    with open(new_filename, 'wb') as f:
        f.write(r.content)
    if fileTypeCheck(new_filename):
        if algorithm != "esrgan":    
            Algol_name = "improved Using " + algorithm
            result_image[Algol_name] = ip(new_filename, algorithm)
            return render_template("Improved_image.html", user_image = result_image)
        else:
            result_image["Improved Using ESRGAN"] = esrgans(new_filename)
            return render_template("Improved_image.html", user_image = result_image) 
    else:
        return render_template("error.html")
@app.route('/sr')
def sr():
    return render_template("sr.html")
@app.route('/about')
def about():
    return render_template("about.html")
if __name__:
    app.run(debug=True)         #use app.run() if you decide to use expose the local server to the internet
                                #also make a change on the begging of the code where flask_ngrok is mentioned
