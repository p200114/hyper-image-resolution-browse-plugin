import numpy as np
from ISR.models import RDN
from ISR.models import RRDN
from PIL import Image
import os
import cv2
import random, string, filetype, imghdr, requests
from urllib.request import urlopen, URLError
import config as cfg
from validator_collection import validators, checkers

class ISR:

     def image_improve_all(image_file_original, algorithm):
          original_image = image_file_original

          if algorithm == "gans":
               locations = os.path.join(cfg.result_folder_gan)
               gan_filename = os.path.join(locations, os.path.basename(original_image))
               actual_location_gan = os.path.join(cfg.actual_location_gan, os.path.basename(original_image))
               img = Image.open(original_image)
               lr_img = np.array(img)
               model = RRDN(weights='gans')
                    
               sr_img = model.predict(lr_img, by_patch_of_size=50)
               gans_image = Image.fromarray(sr_img)
               gans_image.save(gan_filename)
               return actual_location_gan

          if algorithm == "psnrlr":
               locations = os.path.join(cfg.result_folder_psnrlr)
               basic_filename = os.path.join(locations, os.path.basename(original_image))
               actual_location_psnrlr = os.path.join(cfg.actual_location_psnrlr, os.path.basename(original_image))
               
               img = Image.open(original_image)
               lr_img = np.array(img)
               model = RDN(weights='psnr-large')
               sr_img = model.predict(lr_img, by_patch_of_size=50)
                    
               basic_img = Image.fromarray(sr_img)
               basic_img.save(basic_filename)
               return actual_location_psnrlr

          if algorithm == "noise":
               locations = os.path.join(cfg.result_folder_noise)
               noise_filename = os.path.join(locations, os.path.basename(original_image))
               actual_location_noise = os.path.join(cfg.actual_location_noise, os.path.basename(original_image))
               img = Image.open(original_image)
               lr_img = np.array(img)
               model = RDN(weights='noise-cancel')
               sr_img = model.predict(lr_img, by_patch_of_size=50)
                    
               noise_img = Image.fromarray(sr_img)
               noise_img.save(noise_filename)
               
               return actual_location_noise
          
          if algorithm == "psnrsm":
               locations = os.path.join(cfg.result_folder_psnrsm)
               basic_filename = os.path.join(locations, os.path.basename(original_image))
               actual_location_psnrsm = os.path.join(cfg.actual_location_psnrsm, os.path.basename(original_image))
               img = Image.open(original_image)
               lr_img = np.array(img)
               model = RDN(weights='psnr-small')
               sr_img = model.predict(lr_img, by_patch_of_size=50)

               basic_img = Image.fromarray(sr_img)
               basic_img.save(basic_filename)
               return actual_location_psnrsm


     def check_file_type(image_file_location):
               if filetype.is_image(image_file_location):
                    return True
               else:
                    return False
     def url_check(url):
               if checkers.is_url(url):
                    return True
               else:
                    return False
               