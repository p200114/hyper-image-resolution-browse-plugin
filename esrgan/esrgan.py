import os.path as osp
import glob
import cv2
import numpy as np
import torch
import esrgan.RRDBNet_arch as arch

class Esrgan:
#model_path = 'models/RRDB_ESRGAN_x4.pth'  # models/RRDB_ESRGAN_x4.pth OR models/RRDB_PSNR_x4.pth
#device = torch.device('cuda')  # if you want to run on CPU, change 'cuda' -> cpu

        
    def image_improve_esrgan(image_path):
        lr_img = image_path 
        device = torch.device('cpu')
        model_path = "/esrgan/models/RRDB_ESRGAN_x4.pth"
        model = arch.RRDBNet(3, 3, 64, 23, gc=32)
        #model.load_state_dict(torch.load(model_path), strict=True)
        model.load_state_dict(torch.load(path), strict=True)
        model.eval()
        model = model.to(device)

            
        base = osp.splitext(osp.basename(lr_img))[0]
        # read images
        img = cv2.imread(lr_img, cv2.IMREAD_COLOR)
        img = img * 1.0 / 255
        img = torch.from_numpy(np.transpose(img[:, :, [2, 1, 0]], (2, 0, 1))).float()
        img_LR = img.unsqueeze(0)
        img_LR = img_LR.to(device)

        with torch.no_grad():
            output = model(img_LR).data.squeeze().float().cpu().clamp_(0, 1).numpy()
        output = np.transpose(output[[2, 1, 0], :, :], (1, 2, 0))
        output = (output * 255.0).round()
        location = '/static/results/esrgan/{:s}_rlt.png'
        final_loc = location.format(base)
        print(location)
        cv2.imwrite(final_loc, output)
        return final_loc

